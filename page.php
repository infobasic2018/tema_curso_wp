<?php
    get_header( );
?>

    <?php
        while(have_posts()){
            the_post();
            
            ?>
        <div class="jumbotron">
            <h1>
                <?=the_title(  )?>
            </h1>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, vel.
            </p>
        </div>
        <div class="container">
            <?php the_content(  )?>
        </div>
        <?php
        }
    ?>
    
<?php
    get_footer( );
?>